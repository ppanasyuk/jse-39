package ru.t1.panasyuk.tm.endpoint;

import io.qameta.allure.junit4.DisplayName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.panasyuk.tm.api.endpoint.IAuthEndpoint;
import ru.t1.panasyuk.tm.api.endpoint.IDomainEndpoint;
import ru.t1.panasyuk.tm.api.service.IPropertyService;
import ru.t1.panasyuk.tm.dto.request.data.*;
import ru.t1.panasyuk.tm.dto.request.user.UserLoginRequest;
import ru.t1.panasyuk.tm.dto.response.user.UserLoginResponse;
import ru.t1.panasyuk.tm.marker.SoapCategory;
import ru.t1.panasyuk.tm.service.PropertyService;

@Category(SoapCategory.class)
@DisplayName("Тестирование эндпоинта Domain")
public class DomainEndpointTest {

    @NotNull
    private static IDomainEndpoint domainEndpoint;

    @Nullable
    private static String adminToken;

    @BeforeClass
    public static void initSettings() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);
        domainEndpoint = IDomainEndpoint.newInstance(propertyService);
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest("SADMIN", "SADMIN");
        @NotNull final UserLoginResponse response = authEndpoint.loginUser(loginRequest);
        adminToken = response.getToken();
    }

    @Test
    @DisplayName("Сохранение и загрузка бэкапа")
    public void backupTest() {
        @NotNull final DataBackupSaveRequest backupSaveRequest = new DataBackupSaveRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.saveDataBackup(backupSaveRequest));
        @NotNull final DataBackupLoadRequest backupLoadRequest = new DataBackupLoadRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.loadDataBackup(backupLoadRequest));
    }

    @Test
    @DisplayName("Сохранение и загрузка данных в формате base64")
    public void base64Test() {
        @NotNull final DataBase64SaveRequest base64SaveRequest = new DataBase64SaveRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.saveDataBase64(base64SaveRequest));
        @NotNull final DataBase64LoadRequest base64LoadRequest = new DataBase64LoadRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.loadDataBase64(base64LoadRequest));
    }

    @Test
    @DisplayName("Сохранение и загрузка данных в формате binary")
    public void binaryTest() {
        @NotNull final DataBinarySaveRequest binarySaveRequest = new DataBinarySaveRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.saveDataBinary(binarySaveRequest));
        @NotNull final DataBinaryLoadRequest binaryLoadRequest = new DataBinaryLoadRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.loadDataBinary(binaryLoadRequest));
    }

    @Test
    @DisplayName("Сохранение и загрузка данных в формате json используя FasterXML")
    public void jsonFasterXMLTest() {
        @NotNull final DataJsonSaveFasterXMLRequest jsonSaveFasterXMLRequest = new DataJsonSaveFasterXMLRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.saveDataJsonFasterXML(jsonSaveFasterXMLRequest));
        @NotNull final DataJsonLoadFasterXMLRequest jsonLoadFasterRequest = new DataJsonLoadFasterXMLRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.loadDataJsonFasterXML(jsonLoadFasterRequest));
    }

    @Test
    @DisplayName("Сохранение и загрузка данных в формате json используя JaxB")
    public void jsonJaxBTest() {
        @NotNull final DataJsonSaveJaxBRequest jsonSaveJaxBRequest = new DataJsonSaveJaxBRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.saveDataJsonJaxB(jsonSaveJaxBRequest));
        @NotNull final DataJsonLoadJaxBRequest jsonLoadJaxBRequest = new DataJsonLoadJaxBRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.loadDataJsonJaxB(jsonLoadJaxBRequest));
    }

    @Test
    @DisplayName("Сохранение и загрузка данных в формате XML используя FasterXML")
    public void xmlFasterXMLTest() {
        @NotNull final DataXMLSaveFasterXMLRequest xmlSaveFasterXMLRequest = new DataXMLSaveFasterXMLRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.saveDataXMLFasterXML(xmlSaveFasterXMLRequest));
        @NotNull final DataXMLLoadFasterXMLRequest xmlLoadFasterRequest = new DataXMLLoadFasterXMLRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.loadDataXMLFasterXML(xmlLoadFasterRequest));
    }

    @Test
    @DisplayName("Сохранение и загрузка данных в формате XML используя JaxB")
    public void xmlJaxBTest() {
        @NotNull final DataXMLSaveJaxBRequest xmlSaveJaxBRequest = new DataXMLSaveJaxBRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.saveDataXMLJaxB(xmlSaveJaxBRequest));
        @NotNull final DataXMLLoadJaxBRequest xmlLoadJaxBRequest = new DataXMLLoadJaxBRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.loadDataXMLJaxB(xmlLoadJaxBRequest));
    }

    @Test
    @DisplayName("Сохранение и загрузка данных в формате Yaml используя FasterXML")
    public void yamlFasterXMLTest() {
        @NotNull final DataYamlSaveFasterXMLRequest yamlSaveFasterXMLRequest = new DataYamlSaveFasterXMLRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.saveDataYamlFasterXML(yamlSaveFasterXMLRequest));
        @NotNull final DataYamlLoadFasterXMLRequest yamlLoadFasterRequest = new DataYamlLoadFasterXMLRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.loadDataYamlFasterXML(yamlLoadFasterRequest));
    }

}