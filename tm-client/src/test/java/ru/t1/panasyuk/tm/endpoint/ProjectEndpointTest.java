package ru.t1.panasyuk.tm.endpoint;

import io.qameta.allure.junit4.DisplayName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.panasyuk.tm.api.endpoint.IAuthEndpoint;
import ru.t1.panasyuk.tm.api.endpoint.IProjectEndpoint;
import ru.t1.panasyuk.tm.api.endpoint.IUserEndpoint;
import ru.t1.panasyuk.tm.api.service.IPropertyService;
import ru.t1.panasyuk.tm.dto.request.project.*;
import ru.t1.panasyuk.tm.dto.request.user.UserLoginRequest;
import ru.t1.panasyuk.tm.dto.request.user.UserRegistryRequest;
import ru.t1.panasyuk.tm.dto.request.user.UserRemoveRequest;
import ru.t1.panasyuk.tm.dto.response.project.ProjectCreateResponse;
import ru.t1.panasyuk.tm.dto.response.project.ProjectListResponse;
import ru.t1.panasyuk.tm.dto.response.user.UserLoginResponse;
import ru.t1.panasyuk.tm.enumerated.Sort;
import ru.t1.panasyuk.tm.enumerated.Status;
import ru.t1.panasyuk.tm.marker.SoapCategory;
import ru.t1.panasyuk.tm.model.Project;
import ru.t1.panasyuk.tm.service.PropertyService;

import java.util.List;

@Category(SoapCategory.class)
@DisplayName("Тестирование эндпоинта Project")
public class ProjectEndpointTest {

    @NotNull
    private static IUserEndpoint userEndpoint;

    @Nullable
    private static String adminToken;

    @Nullable
    private static String userToken;

    @NotNull
    private static IProjectEndpoint projectEndpoint;

    @NotNull
    private static IPropertyService propertyService;

    @NotNull
    private String firstProjectId;

    @NotNull
    private String secondProjectId;

    @BeforeClass
    public static void initUser() {
        propertyService = new PropertyService();
        projectEndpoint = IProjectEndpoint.newInstance(propertyService);
        @NotNull final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);
        userEndpoint = IUserEndpoint.newInstance(propertyService);
        @NotNull UserLoginRequest loginRequest = new UserLoginRequest("SADMIN", "SADMIN");
        @NotNull UserLoginResponse response = authEndpoint.loginUser(loginRequest);
        adminToken = response.getToken();

        @NotNull final UserRegistryRequest registryRequest = new UserRegistryRequest("TEST", "TEST", "TEST@TEST");
        userEndpoint.registryUser(registryRequest);
        loginRequest = new UserLoginRequest("TEST", "TEST");
        response = authEndpoint.loginUser(loginRequest);
        userToken = response.getToken();
    }

    @AfterClass
    public static void dropUser() {
        @NotNull final UserRemoveRequest removeRequest = new UserRemoveRequest(adminToken, "TEST");
        userEndpoint.removeUser(removeRequest);
    }

    @Before
    public void initData() {
        @NotNull ProjectCreateRequest createRequest = new ProjectCreateRequest(userToken, "Test Project 1", "Desc");
        firstProjectId = projectEndpoint.createProject(createRequest).getProject().getId();
        createRequest = new ProjectCreateRequest(userToken, "Test Project 2", "Desc");
        secondProjectId = projectEndpoint.createProject(createRequest).getProject().getId();
    }

    @After
    public void dropData() {
        @NotNull final ProjectClearRequest clearRequest = new ProjectClearRequest(userToken);
        projectEndpoint.clearProject(clearRequest);
    }

    @Nullable
    private Project findProjectById(@NotNull final String projectId) {
        @NotNull final ProjectFindOneByIdRequest request = new ProjectFindOneByIdRequest(userToken, projectId);
        return projectEndpoint.findProjectById(request).getProject();
    }

    @Nullable
    private Project findProjectByIndex(@NotNull final Integer index) {
        @NotNull final ProjectFindOneByIndexRequest request = new ProjectFindOneByIndexRequest(userToken, index);
        return projectEndpoint.findProjectByIndex(request).getProject();
    }

    @Test
    @DisplayName("Изменение статуса проекта по Id")
    public void changeStatusByIdTest() {
        @NotNull final Status status = Status.IN_PROGRESS;
        @NotNull final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(userToken, firstProjectId, status);
        Assert.assertNotNull(projectEndpoint.changeProjectStatusById(request));
        @Nullable final Project project = findProjectById(firstProjectId);
        Assert.assertNotNull(project);
        Assert.assertEquals(status, project.getStatus());
    }

    @Test(expected = Exception.class)
    @DisplayName("Изменение статуса проекта по Id без токена")
    public void changeStatusByIdTestNegative() {
        @NotNull final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(null, firstProjectId, Status.IN_PROGRESS);
        projectEndpoint.changeProjectStatusById(request);
    }

    @Test
    @DisplayName("Изменение статуса проекта по индексу")
    public void changeStatusByIndexTest() {
        @NotNull final Status status = Status.IN_PROGRESS;
        @NotNull final ProjectChangeStatusByIndexRequest request = new ProjectChangeStatusByIndexRequest(userToken, 0, status);
        Assert.assertNotNull(projectEndpoint.changeProjectStatusByIndex(request));
        @Nullable final Project project = findProjectByIndex(0);
        Assert.assertNotNull(project);
        Assert.assertEquals(status, project.getStatus());
    }

    @Test(expected = Exception.class)
    @DisplayName("Изменение статуса проекта по индексу без токена")
    public void changeStatusByIndexTestNegative() {
        @NotNull final ProjectChangeStatusByIndexRequest request = new ProjectChangeStatusByIndexRequest(null, 0, Status.IN_PROGRESS);
        projectEndpoint.changeProjectStatusByIndex(request);
    }

    @Test
    @DisplayName("Удаление проектов")
    public void clearTest() {
        @NotNull final ProjectClearRequest request = new ProjectClearRequest(userToken);
        Assert.assertNotNull(projectEndpoint.clearProject(request));
        @NotNull final ProjectListRequest listRequest = new ProjectListRequest(userToken, Sort.BY_NAME);
        @Nullable final List<Project> projects = projectEndpoint.listProject(listRequest).getProjects();
        Assert.assertNull(projects);
    }

    @Test(expected = Exception.class)
    @DisplayName("Удаление проектов без токена")
    public void clearTestNegative() {
        @NotNull final ProjectClearRequest request = new ProjectClearRequest(null);
        projectEndpoint.clearProject(request);
    }

    @Test
    @DisplayName("Изменение статуса проекта на Completed по Id")
    public void completeByIdTest() {
        @NotNull final ProjectCompleteByIdRequest request = new ProjectCompleteByIdRequest(userToken, secondProjectId);
        Assert.assertNotNull(projectEndpoint.completeProjectById(request));
        @Nullable final Project project = findProjectById(secondProjectId);
        Assert.assertNotNull(project);
        Assert.assertEquals(Status.COMPLETED, project.getStatus());
    }

    @Test(expected = Exception.class)
    @DisplayName("Изменение статуса проекта на Completed по Id без токена")
    public void completeByIdTestNegative() {
        @NotNull final ProjectCompleteByIdRequest request = new ProjectCompleteByIdRequest(null, secondProjectId);
        projectEndpoint.completeProjectById(request);
    }

    @Test
    @DisplayName("Изменение статуса проекта на Completed по индексу")
    public void completeByIndexTest() {
        @NotNull final ProjectCompleteByIndexRequest request = new ProjectCompleteByIndexRequest(userToken, 0);
        Assert.assertNotNull(projectEndpoint.completeProjectByIndex(request));
        @Nullable final Project project = findProjectByIndex(0);
        Assert.assertNotNull(project);
        Assert.assertEquals(Status.COMPLETED, project.getStatus());
    }

    @Test(expected = Exception.class)
    @DisplayName("Изменение статуса проекта на Completed по индексу без токена")
    public void completeByIndexTestNegative() {
        @NotNull final ProjectCompleteByIndexRequest request = new ProjectCompleteByIndexRequest(null, 0);
        projectEndpoint.completeProjectByIndex(request);
    }

    @Test
    @DisplayName("Создание проекта")
    public void createTest() {
        @NotNull final String name = "NAME TEST CREATE";
        @NotNull final String desc = "DESC TEST CREATE";
        @NotNull final ProjectCreateRequest request = new ProjectCreateRequest(userToken, name, desc);
        @Nullable final ProjectCreateResponse response = projectEndpoint.createProject(request);
        Assert.assertNotNull(response);
        @Nullable final Project returnedProject = response.getProject();
        Assert.assertNotNull(returnedProject);
        @NotNull final String projectId = returnedProject.getId();
        @Nullable final Project createdProject = findProjectById(projectId);
        Assert.assertNotNull(createdProject);
        Assert.assertEquals(name, createdProject.getName());
        Assert.assertEquals(desc, createdProject.getDescription());
    }

    @Test(expected = Exception.class)
    @DisplayName("Создание проекта без токена")
    public void createTestNegative() {
        @NotNull final ProjectCreateRequest request = new ProjectCreateRequest(null, "NAME", "DESC");
        projectEndpoint.createProject(request);
    }

    @Test
    @DisplayName("Поиск проекта по Id")
    public void findOneByIdTest() {
        @NotNull final ProjectFindOneByIdRequest request = new ProjectFindOneByIdRequest(userToken, firstProjectId);
        @Nullable final Project project = projectEndpoint.findProjectById(request).getProject();
        Assert.assertNotNull(project);
    }

    @Test(expected = Exception.class)
    @DisplayName("Поиск проекта по Id без токена")
    public void findOneByIdTestNegative() {
        @NotNull final ProjectFindOneByIdRequest request = new ProjectFindOneByIdRequest(null, firstProjectId);
        projectEndpoint.findProjectById(request);
    }

    @Test
    @DisplayName("Поиск проекта по индексу")
    public void findOneByIndexTest() {
        @NotNull final ProjectFindOneByIndexRequest request = new ProjectFindOneByIndexRequest(userToken, 0);
        @Nullable final Project project = projectEndpoint.findProjectByIndex(request).getProject();
        Assert.assertNotNull(project);
    }

    @Test(expected = Exception.class)
    @DisplayName("Поиск проекта по индексу без токена")
    public void findOneByIndexTestNegative() {
        @NotNull final ProjectFindOneByIndexRequest request = new ProjectFindOneByIndexRequest(null, 0);
        projectEndpoint.findProjectByIndex(request);
    }

    @Test
    @DisplayName("Получение списка проектов")
    public void listTest() {
        @NotNull final ProjectListRequest request = new ProjectListRequest(userToken, Sort.BY_NAME);
        @Nullable final ProjectListResponse response = projectEndpoint.listProject(request);
        Assert.assertNotNull(response);
        @Nullable final List<Project> projects = response.getProjects();
        Assert.assertNotNull(projects);
        Assert.assertTrue(projects.size() > 0);
        Assert.assertEquals(firstProjectId, projects.get(0).getId());
        Assert.assertEquals(secondProjectId, projects.get(1).getId());
    }

    @Test(expected = Exception.class)
    @DisplayName("Получение списка проектов без токена")
    public void listTestNegative() {
        @NotNull final ProjectListRequest request = new ProjectListRequest(null, Sort.BY_NAME);
        projectEndpoint.listProject(request);
    }

    @Test
    @DisplayName("Удаление проекта по Id")
    public void removeByIdTest() {
        @NotNull final ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest(userToken, firstProjectId);
        Assert.assertNotNull(projectEndpoint.removeProjectById(request));
        @Nullable final Project project = findProjectById(firstProjectId);
        Assert.assertNull(project);
    }

    @Test(expected = Exception.class)
    @DisplayName("Удаление проекта по Id без токена")
    public void removeByIdTestNegative() {
        @NotNull final ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest(null, firstProjectId);
        projectEndpoint.removeProjectById(request);
    }

    @Test
    @DisplayName("Удаление проекта по индексу")
    public void removeByIndexTest() {
        @NotNull final ProjectRemoveByIndexRequest request = new ProjectRemoveByIndexRequest(userToken, 0);
        Assert.assertNotNull(projectEndpoint.removeProjectByIndex(request));
        @Nullable final Project project = findProjectByIndex(0);
        Assert.assertNotNull(project);
        Assert.assertNotEquals(firstProjectId, project.getId());
    }

    @Test(expected = Exception.class)
    @DisplayName("Удаление проекта по индексу без токена")
    public void removeByIndexTestNegative() {
        @NotNull final ProjectRemoveByIndexRequest request = new ProjectRemoveByIndexRequest(null, 0);
        projectEndpoint.removeProjectByIndex(request);
    }

    @Test
    @DisplayName("Изменени статуса проекта на In Progress по Id")
    public void startByIdTest() {
        @NotNull final ProjectStartByIdRequest request = new ProjectStartByIdRequest(userToken, secondProjectId);
        Assert.assertNotNull(projectEndpoint.startProjectById(request));
        @Nullable final Project project = findProjectById(secondProjectId);
        Assert.assertNotNull(project);
        Assert.assertEquals(Status.IN_PROGRESS, project.getStatus());
    }

    @Test(expected = Exception.class)
    @DisplayName("Изменени статуса проекта на In Progress по Id без токена")
    public void startByIdTestNegative() {
        @NotNull final ProjectStartByIdRequest request = new ProjectStartByIdRequest(null, secondProjectId);
        projectEndpoint.startProjectById(request);
    }

    @Test
    @DisplayName("Изменени статуса проекта на In Progress по индексу")
    public void startByIndexTest() {
        @NotNull final ProjectStartByIndexRequest request = new ProjectStartByIndexRequest(userToken, 0);
        Assert.assertNotNull(projectEndpoint.startProjectByIndex(request));
        @Nullable final Project project = findProjectByIndex(0);
        Assert.assertNotNull(project);
        Assert.assertEquals(Status.IN_PROGRESS, project.getStatus());
    }

    @Test(expected = Exception.class)
    @DisplayName("Изменени статуса проекта на In Progress по индексу без токена")
    public void startByIndexTestNegative() {
        @NotNull final ProjectStartByIndexRequest request = new ProjectStartByIndexRequest(null, 0);
        projectEndpoint.startProjectByIndex(request);
    }

    @Test
    @DisplayName("Изменение проекта по Id")
    public void updateByIdTest() {
        @NotNull final String name = "NAME TEST UPDATE";
        @NotNull final String desc = "DESC TEST UPDATE";
        @NotNull final ProjectUpdateByIdRequest request = new ProjectUpdateByIdRequest(userToken, firstProjectId, name, desc);
        Assert.assertNotNull(projectEndpoint.updateProjectById(request));
        @Nullable final Project updatedProject = findProjectById(firstProjectId);
        Assert.assertNotNull(updatedProject);
        Assert.assertEquals(name, updatedProject.getName());
        Assert.assertEquals(desc, updatedProject.getDescription());
    }

    @Test(expected = Exception.class)
    @DisplayName("Изменение проекта по Id без токена")
    public void updateByIdTestNegative() {
        @NotNull final ProjectUpdateByIdRequest request = new ProjectUpdateByIdRequest(null, firstProjectId, "NAME", "DESC");
        projectEndpoint.updateProjectById(request);
    }

    @Test
    @DisplayName("Изменение проекта по индексу")
    public void updateByIndexTest() {
        @NotNull final String name = "NAME TEST UPDATE";
        @NotNull final String desc = "DESC TEST UPDATE";
        @NotNull final ProjectUpdateByIndexRequest request = new ProjectUpdateByIndexRequest(userToken, 0, name, desc);
        Assert.assertNotNull(projectEndpoint.updateProjectByIndex(request));
        @Nullable final Project updatedProject = findProjectByIndex(0);
        Assert.assertNotNull(updatedProject);
        Assert.assertEquals(name, updatedProject.getName());
        Assert.assertEquals(desc, updatedProject.getDescription());
    }

    @Test(expected = Exception.class)
    @DisplayName("Изменение проекта по индексу без токена")
    public void updateByIndexTestNegative() {
        @NotNull final ProjectUpdateByIndexRequest request = new ProjectUpdateByIndexRequest(null, 0, "NAME", "DESC");
        projectEndpoint.updateProjectByIndex(request);
    }

}