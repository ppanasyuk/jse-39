package ru.t1.panasyuk.tm;

import ru.t1.panasyuk.tm.component.Bootstrap;

public final class Application {

    public static void main(final String... args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}