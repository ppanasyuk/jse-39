package ru.t1.panasyuk.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.dto.request.project.ProjectRemoveByIdRequest;
import ru.t1.panasyuk.tm.dto.request.project.ProjectStartByIdRequest;
import ru.t1.panasyuk.tm.dto.response.project.ProjectStartByIdResponse;
import ru.t1.panasyuk.tm.enumerated.Status;
import ru.t1.panasyuk.tm.util.TerminalUtil;

public final class ProjectStartByIdCommand extends AbstractProjectCommand {

    @NotNull
    private static final String DESCRIPTION = "Start project by id.";

    @NotNull
    private static final String NAME = "project-start-by-id";

    @Override
    public void execute() {
        System.out.println("[START PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectStartByIdRequest request = new ProjectStartByIdRequest(getToken(), id);
        @NotNull final ProjectStartByIdResponse response = getProjectEndpoint().startProjectById(request);
        if (!response.getSuccess()) System.out.println(response.getMessage());
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}