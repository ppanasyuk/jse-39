package ru.t1.panasyuk.tm.service;

import io.qameta.allure.junit4.DisplayName;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import ru.t1.panasyuk.tm.api.service.*;
import ru.t1.panasyuk.tm.enumerated.Role;
import ru.t1.panasyuk.tm.model.Project;
import ru.t1.panasyuk.tm.model.Task;
import ru.t1.panasyuk.tm.model.User;

import java.util.List;

@DisplayName("Тестирование сервиса DomainService")
public class DomainServiceTest {

    @NotNull
    private IServiceLocator serviceLocator;

    @NotNull
    private User test;

    @NotNull
    private User admin;

    @NotNull
    private static IConnectionService connectionService;

    @BeforeClass
    public static void initConnection() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        connectionService = new ConnectionService(propertyService);
    }

    @Before
    public void initService() throws Exception {
        serviceLocator = new IServiceLocator() {

            @Getter
            @NotNull
            final IPropertyService propertyService = new PropertyService();

            @Getter
            @NotNull
            final IProjectService projectService = new ProjectService(connectionService);

            @Getter
            @NotNull
            final ITaskService taskService = new TaskService(connectionService);

            @Getter
            @NotNull
            final IProjectTaskService projectTaskService = new ProjectTaskService(connectionService);

            @Getter
            @NotNull
            final IUserService userService = new UserService(propertyService, connectionService, projectTaskService);

            @Getter
            @NotNull
            final IDomainService domainService = new DomainService(this);

            @Getter
            @NotNull
            final ISessionService sessionService = new SessionService(connectionService);

            @Getter
            @NotNull
            final IAuthService authService = new AuthService(propertyService, userService, sessionService);

            @Getter
            @NotNull
            final ILoggerService loggerService = new LoggerService(propertyService);

        };

        test = serviceLocator.getUserService().create("TEST", "TEST", "TEST@TEST.ru");
        admin = serviceLocator.getUserService().create("ADMIN", "ADMIN", Role.ADMIN);
        @NotNull final Project project1 = serviceLocator.getProjectService().create(test.getId(), "Project 1", "Project for TEST");
        @NotNull final Project project2 = serviceLocator.getProjectService().create(admin.getId(), "Project 2", "Project for ADMIN");
        @NotNull final Project project3 = serviceLocator.getProjectService().create(test.getId(), "Project 3", "Project for TEST 2");
        @NotNull final Task task1 = serviceLocator.getTaskService().create(test.getId(), "Task 1", "Task for project 1");
        task1.setProjectId(project1.getId());
        serviceLocator.getTaskService().update(task1);
        @NotNull final Task task2 = serviceLocator.getTaskService().create(test.getId(), "Task 2", "Task for project 1");
        task2.setProjectId(project1.getId());
        serviceLocator.getTaskService().update(task2);
        @NotNull final Task task3 = serviceLocator.getTaskService().create(admin.getId(), "Task 3", "Task for project 2");
        task3.setProjectId(project2.getId());
        serviceLocator.getTaskService().update(task3);
        @NotNull final Task task4 = serviceLocator.getTaskService().create(admin.getId(), "Task 4", "Task for project 2");
        task4.setProjectId(project2.getId());
        serviceLocator.getTaskService().update(task4);
    }

    @After
    public void afterTest() throws Exception {
        serviceLocator.getTaskService().clear(test.getId());
        serviceLocator.getTaskService().clear(admin.getId());
        serviceLocator.getProjectService().clear(test.getId());
        serviceLocator.getProjectService().clear(admin.getId());
        serviceLocator.getUserService().remove(admin);
        serviceLocator.getUserService().remove(test);
    }

    @Test
    @DisplayName("Сохранение и загрузка бэкапа")
    public void dataBackupTest() throws Exception {
        @Nullable final List<Project> projects = serviceLocator.getProjectService().findAll(test.getId());
        Assert.assertNotNull(projects);
        Assert.assertTrue(projects.size() > 0);
        @Nullable final List<Task> tasks = serviceLocator.getTaskService().findAll(test.getId());
        Assert.assertNotNull(tasks);
        Assert.assertTrue(tasks.size() > 0);
        @NotNull final List<User> users = serviceLocator.getUserService().findAll();
        Assert.assertTrue(users.size() > 0);
        serviceLocator.getDomainService().saveDataBackup();
        serviceLocator.getProjectService().clear();
        serviceLocator.getTaskService().clear();
        serviceLocator.getUserService().clear();
        Assert.assertEquals(0, serviceLocator.getProjectService().getSize(test.getId()));
        Assert.assertEquals(0, serviceLocator.getTaskService().getSize(test.getId()));
        Assert.assertEquals(0, serviceLocator.getUserService().getSize());
        serviceLocator.getDomainService().loadDataBackup();
        Assert.assertEquals(projects, serviceLocator.getProjectService().findAll(test.getId()));
        Assert.assertEquals(tasks, serviceLocator.getTaskService().findAll(test.getId()));
        Assert.assertEquals(users, serviceLocator.getUserService().findAll());
    }

    @Test
    @DisplayName("Сохранение и загрузка данных в фомате Base64")
    public void dataBase64Test() throws Exception {
        @Nullable final List<Project> projects = serviceLocator.getProjectService().findAll(test.getId());
        Assert.assertNotNull(projects);
        Assert.assertTrue(projects.size() > 0);
        @Nullable final List<Task> tasks = serviceLocator.getTaskService().findAll(test.getId());
        Assert.assertNotNull(tasks);
        Assert.assertTrue(tasks.size() > 0);
        @NotNull final List<User> users = serviceLocator.getUserService().findAll();
        Assert.assertTrue(users.size() > 0);
        serviceLocator.getDomainService().saveDataBase64();
        serviceLocator.getProjectService().clear(test.getId());
        serviceLocator.getTaskService().clear(test.getId());
        serviceLocator.getUserService().clear();
        Assert.assertEquals(0, serviceLocator.getProjectService().getSize(test.getId()));
        Assert.assertEquals(0, serviceLocator.getTaskService().getSize(test.getId()));
        Assert.assertEquals(0, serviceLocator.getUserService().getSize());
        serviceLocator.getDomainService().loadDataBase64();
        Assert.assertEquals(projects, serviceLocator.getProjectService().findAll(test.getId()));
        Assert.assertEquals(tasks, serviceLocator.getTaskService().findAll(test.getId()));
        Assert.assertEquals(users, serviceLocator.getUserService().findAll());
    }

    @Test
    @DisplayName("Сохранение и загрузка данных в фомате Binary")
    public void dataBinaryTest() throws Exception {
        @Nullable final List<Project> projects = serviceLocator.getProjectService().findAll(test.getId());
        Assert.assertNotNull(projects);
        Assert.assertTrue(projects.size() > 0);
        @Nullable final List<Task> tasks = serviceLocator.getTaskService().findAll(test.getId());
        Assert.assertNotNull(tasks);
        Assert.assertTrue(tasks.size() > 0);
        @NotNull final List<User> users = serviceLocator.getUserService().findAll();
        Assert.assertTrue(users.size() > 0);
        serviceLocator.getDomainService().saveDataBinary();
        serviceLocator.getProjectService().clear(test.getId());
        serviceLocator.getTaskService().clear(test.getId());
        serviceLocator.getUserService().clear();
        Assert.assertEquals(0, serviceLocator.getProjectService().getSize(test.getId()));
        Assert.assertEquals(0, serviceLocator.getTaskService().getSize(test.getId()));
        Assert.assertEquals(0, serviceLocator.getUserService().getSize());
        serviceLocator.getDomainService().loadDataBinary();
        Assert.assertEquals(projects, serviceLocator.getProjectService().findAll(test.getId()));
        Assert.assertEquals(tasks, serviceLocator.getTaskService().findAll(test.getId()));
        Assert.assertEquals(users, serviceLocator.getUserService().findAll());
    }

    @Test
    @DisplayName("Сохранение и загрузка данных в фомате Json используя FasterXML")
    public void dataJsonFasterXMLTest() throws Exception {
        @Nullable final List<Project> projects = serviceLocator.getProjectService().findAll(test.getId());
        Assert.assertNotNull(projects);
        Assert.assertTrue(projects.size() > 0);
        @Nullable final List<Task> tasks = serviceLocator.getTaskService().findAll(test.getId());
        Assert.assertNotNull(tasks);
        Assert.assertTrue(tasks.size() > 0);
        @NotNull final List<User> users = serviceLocator.getUserService().findAll();
        Assert.assertTrue(users.size() > 0);
        serviceLocator.getDomainService().saveDataJsonFasterXML();
        serviceLocator.getProjectService().clear(test.getId());
        serviceLocator.getTaskService().clear(test.getId());
        serviceLocator.getUserService().clear();
        Assert.assertEquals(0, serviceLocator.getProjectService().getSize(test.getId()));
        Assert.assertEquals(0, serviceLocator.getTaskService().getSize(test.getId()));
        Assert.assertEquals(0, serviceLocator.getUserService().getSize());
        serviceLocator.getDomainService().loadDataJsonFasterXML();
        Assert.assertEquals(projects, serviceLocator.getProjectService().findAll(test.getId()));
        Assert.assertEquals(tasks, serviceLocator.getTaskService().findAll(test.getId()));
        Assert.assertEquals(users, serviceLocator.getUserService().findAll());
    }

    @Test
    @DisplayName("Сохранение и загрузка данных в фомате Json используя JaxB")
    public void dataJsonJaxBTest() throws Exception {
        @Nullable final List<Project> projects = serviceLocator.getProjectService().findAll(test.getId());
        Assert.assertNotNull(projects);
        Assert.assertTrue(projects.size() > 0);
        @Nullable final List<Task> tasks = serviceLocator.getTaskService().findAll(test.getId());
        Assert.assertNotNull(tasks);
        Assert.assertTrue(tasks.size() > 0);
        @NotNull final List<User> users = serviceLocator.getUserService().findAll();
        Assert.assertTrue(users.size() > 0);
        serviceLocator.getDomainService().saveDataJsonJaxB();
        serviceLocator.getProjectService().clear(test.getId());
        serviceLocator.getTaskService().clear(test.getId());
        serviceLocator.getUserService().clear();
        Assert.assertEquals(0, serviceLocator.getProjectService().getSize(test.getId()));
        Assert.assertEquals(0, serviceLocator.getTaskService().getSize(test.getId()));
        Assert.assertEquals(0, serviceLocator.getUserService().getSize());
        serviceLocator.getDomainService().loadDataJsonJaxB();
        Assert.assertEquals(projects, serviceLocator.getProjectService().findAll(test.getId()));
        Assert.assertEquals(tasks, serviceLocator.getTaskService().findAll(test.getId()));
        Assert.assertEquals(users, serviceLocator.getUserService().findAll());
    }

    @Test
    @DisplayName("Сохранение и загрузка данных в фомате XML используя FasterXML")
    public void dataXMLFasterXMLTest() throws Exception {
        @Nullable final List<Project> projects = serviceLocator.getProjectService().findAll(test.getId());
        Assert.assertNotNull(projects);
        Assert.assertTrue(projects.size() > 0);
        @Nullable final List<Task> tasks = serviceLocator.getTaskService().findAll(test.getId());
        Assert.assertNotNull(tasks);
        Assert.assertTrue(tasks.size() > 0);
        @NotNull final List<User> users = serviceLocator.getUserService().findAll();
        Assert.assertTrue(users.size() > 0);
        serviceLocator.getDomainService().saveDataXMLFasterXML();
        serviceLocator.getProjectService().clear(test.getId());
        serviceLocator.getTaskService().clear(test.getId());
        serviceLocator.getUserService().clear();
        Assert.assertEquals(0, serviceLocator.getProjectService().getSize(test.getId()));
        Assert.assertEquals(0, serviceLocator.getTaskService().getSize(test.getId()));
        Assert.assertEquals(0, serviceLocator.getUserService().getSize());
        serviceLocator.getDomainService().loadDataXMLFasterXML();
        Assert.assertEquals(projects, serviceLocator.getProjectService().findAll(test.getId()));
        Assert.assertEquals(tasks, serviceLocator.getTaskService().findAll(test.getId()));
        Assert.assertEquals(users, serviceLocator.getUserService().findAll());
    }

    @Test
    @DisplayName("Сохранение и загрузка данных в фомате XML используя JaxB")
    public void dataXMLJaxBTest() throws Exception {
        @Nullable final List<Project> projects = serviceLocator.getProjectService().findAll(test.getId());
        Assert.assertNotNull(projects);
        Assert.assertTrue(projects.size() > 0);
        @Nullable final List<Task> tasks = serviceLocator.getTaskService().findAll(test.getId());
        Assert.assertNotNull(tasks);
        Assert.assertTrue(tasks.size() > 0);
        @NotNull final List<User> users = serviceLocator.getUserService().findAll();
        Assert.assertTrue(users.size() > 0);
        serviceLocator.getDomainService().saveDataXMLJaxB();
        serviceLocator.getProjectService().clear(test.getId());
        serviceLocator.getTaskService().clear(test.getId());
        serviceLocator.getUserService().clear();
        Assert.assertEquals(0, serviceLocator.getProjectService().getSize(test.getId()));
        Assert.assertEquals(0, serviceLocator.getTaskService().getSize(test.getId()));
        Assert.assertEquals(0, serviceLocator.getUserService().getSize());
        serviceLocator.getDomainService().loadDataXMLJaxB();
        Assert.assertEquals(projects, serviceLocator.getProjectService().findAll(test.getId()));
        Assert.assertEquals(tasks, serviceLocator.getTaskService().findAll(test.getId()));
        Assert.assertEquals(users, serviceLocator.getUserService().findAll());
    }

    @Test
    @DisplayName("Сохранение и загрузка данных в фомате Yaml используя FasterXML")
    public void dataYamlFasterXMLTest() throws Exception {
        @Nullable final List<Project> projects = serviceLocator.getProjectService().findAll(test.getId());
        Assert.assertNotNull(projects);
        Assert.assertTrue(projects.size() > 0);
        @Nullable final List<Task> tasks = serviceLocator.getTaskService().findAll(test.getId());
        Assert.assertNotNull(tasks);
        Assert.assertTrue(tasks.size() > 0);
        @NotNull final List<User> users = serviceLocator.getUserService().findAll();
        Assert.assertTrue(users.size() > 0);
        serviceLocator.getDomainService().saveDataYamlFasterXML();
        serviceLocator.getProjectService().clear(test.getId());
        serviceLocator.getTaskService().clear(test.getId());
        serviceLocator.getUserService().clear();
        Assert.assertEquals(0, serviceLocator.getProjectService().getSize(test.getId()));
        Assert.assertEquals(0, serviceLocator.getTaskService().getSize(test.getId()));
        Assert.assertEquals(0, serviceLocator.getUserService().getSize());
        serviceLocator.getDomainService().loadDataYamlFasterXML();
        Assert.assertEquals(projects, serviceLocator.getProjectService().findAll(test.getId()));
        Assert.assertEquals(tasks, serviceLocator.getTaskService().findAll(test.getId()));
        Assert.assertEquals(users, serviceLocator.getUserService().findAll());
    }

}