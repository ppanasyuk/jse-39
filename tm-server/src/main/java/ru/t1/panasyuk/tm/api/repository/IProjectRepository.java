package ru.t1.panasyuk.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    @Insert("INSERT INTO tm_project (id, name, created, status, description, user_id) " +
            "VALUES (#{id}, #{name}, #{created}, #{status}, #{description}, #{userId})")
    void add(@NotNull Project project);

    @Insert("INSERT INTO tm_project (id, name, created, status, description, user_id) " +
            "VALUES (#{id}, #{name}, #{created}, #{status}, #{description}, #{userId})")
    void addForUser(@NotNull @Param("userId") String userId, @NotNull Project project);

    @Delete("DELETE FROM tm_project WHERE user_id = #{userId}")
    void clearForUser(@NotNull @Param("userId") String userId);

    @Delete("DELETE FROM tm_project")
    void clear();

    @Select("SELECT * FROM tm_project ORDER BY created DESC")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable List<Project> findAll();

    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} ORDER BY created DESC")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable List<Project> findAllForUser(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} ORDER BY created")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable List<Project> findAllOrderByCreated(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} ORDER BY name")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable List<Project> findAllOrderByName(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} ORDER BY status")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable List<Project> findAllOrderByStatus(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} AND id = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable Project findOneById(@NotNull @Param("userId") String userId, @Param("id") @Nullable String id);

    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} ORDER BY created DESC LIMIT 1 OFFSET #{index}-1")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable Project findOneByIndex(@NotNull @Param("userId") String userId, @Nullable @Param("index") Integer index);

    @Select("SELECT COUNT(id) FROM tm_project WHERE user_id = #{userId}")
    int getSize(@NotNull String userId);

    @Delete("DELETE FROM tm_project WHERE user_id = #{userId} AND id = #{id}")
    void remove(@Nullable Project project);

    @Update("UPDATE tm_project SET name = #{name}, description = #{description}, status = #{status} " +
            "WHERE user_id = #{userId} AND id = #{id}")
    void update(@NotNull Project project);

}