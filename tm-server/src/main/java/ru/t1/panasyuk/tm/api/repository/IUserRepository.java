package ru.t1.panasyuk.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.model.User;

import java.sql.SQLException;
import java.util.List;

public interface IUserRepository {

    @Insert("INSERT INTO tm_user (id, login, password," +
            " first_name, last_name, middle_name, email, role, locked, created) " +
            "VALUES (#{id}, #{login}, #{passwordHash}," +
            " #{firstName}, #{lastName}, #{middleName}, #{email}, #{role}, #{locked}, #{created})")
    void add(@NotNull User user);

    @Delete("DELETE FROM tm_user")
    void clear();

    @Select("SELECT * FROM tm_user ORDER BY created DESC")
    @Results(value = {
            @Result(property = "passwordHash", column = "password"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    List<User> findAll();

    @Select("SELECT * FROM tm_user WHERE login = #{login} LIMIT 1")
    @Results(value = {
            @Result(property = "passwordHash", column = "password"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    @Nullable User findByLogin(@Nullable @Param("login") String login) throws Exception;

    @Select("SELECT * FROM tm_user WHERE email = #{email} LIMIT 1")
    @Results(value = {
            @Result(property = "passwordHash", column = "password"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    @Nullable User findByEmail(@Nullable @Param("email") String email) throws Exception;

    @Select("SELECT * FROM tm_user WHERE id = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "passwordHash", column = "password"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    User findOneById(@Nullable @Param("id") String id);

    @Select("SELECT * FROM tm_user ORDER BY created DESC LIMIT 1 OFFSET #{index}-1")
    @Results(value = {
            @Result(property = "passwordHash", column = "password"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    User findOneByIndex(@Nullable @Param("index") Integer index);

    @Select("SELECT COUNT(id) FROM tm_user")
    int getSize();

    @Delete("DELETE FROM tm_user WHERE id = #{id}")
    void remove(@Nullable User user);

    @Update("UPDATE tm_user SET login = #{login}, password = #{passwordHash}, email = #{email}, " +
            "last_name = #{lastName}, first_name = #{firstName}, middle_name = #{middleName}, " +
            "locked = #{locked}, role = #{role} WHERE id = #{id}")
    void update(@NotNull User user);

}