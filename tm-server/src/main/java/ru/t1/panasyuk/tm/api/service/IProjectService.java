package ru.t1.panasyuk.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.enumerated.Sort;
import ru.t1.panasyuk.tm.enumerated.Status;
import ru.t1.panasyuk.tm.model.Project;
import ru.t1.panasyuk.tm.model.Task;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IProjectService extends IUserOwnedService<Project> {

    @NotNull
    Project changeProjectStatusById(@NotNull String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    Project changeProjectStatusByIndex(@NotNull String userId, @Nullable Integer index, @Nullable Status status);

    @NotNull
    Project create(@NotNull String userId, @Nullable String name, @Nullable String description);

    @NotNull
    Project create(@NotNull String userId, @Nullable String name);

    void clear();

    List<Project> findAll();

    List<Project> findAll(@NotNull String userId, @Nullable Comparator comparator);

    List<Project> findAll(@NotNull String userId, @Nullable Sort sort);

    Collection<Project> set(@NotNull Collection<Project> projects);

    @NotNull
    Project updateById(@NotNull String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @NotNull
    Project updateByIndex(@NotNull String userId, @Nullable Integer index, @Nullable String name, @Nullable String description);

    void update(@NotNull Project project);

}