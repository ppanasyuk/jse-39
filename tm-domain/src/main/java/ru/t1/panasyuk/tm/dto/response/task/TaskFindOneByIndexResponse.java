package ru.t1.panasyuk.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.model.Task;

@Getter
@Setter
@NoArgsConstructor
public final class TaskFindOneByIndexResponse extends AbstractTaskResponse {

    @Nullable
    private Task task;

    public TaskFindOneByIndexResponse(@Nullable final Task task) {
        this.task = task;
    }

    public TaskFindOneByIndexResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}